function [aveSuccessRatePlot, aveSuccessRatePlotErr] = ...
    genPerfMatOurs(seqs, trackers_ours)

pathAnno = './anno/';
numTrk = length(trackers_ours);

thresholdSetOverlap = 0:0.05:1;
thresholdSetError = 0:50;

rpAll = ['./results/results_Ours/'];

for idxSeq=1:length(seqs)
    s = seqs{idxSeq};
    
    s.len = s.endFrame - s.startFrame + 1;
    s.s_frames = cell(s.len,1);
    nz	= strcat('%0',num2str(s.nz),'d'); %number of zeros in the name of image
    for i=1:s.len
        image_no = s.startFrame + (i-1);
        id = sprintf(nz,image_no);
        s.s_frames{i} = strcat(s.path,id,'.',s.ext);
    end
    
    rect_anno = dlmread([pathAnno s.name '.txt']);
    numSeg = 20;
    [subSeqs, subAnno]=splitSeqTRE(s,numSeg,rect_anno);
    
    nameAll=[];
    for idxTrk=1:numTrk
        
        t = trackers_ours{idxTrk};
        
        rectMat = dlmread([rpAll t '/' s.name '.txt']);
        disp([s.name ' ' t]);
        
        aveCoverageAll=[];
        aveErrCenterAll=[];
        errCvgAccAvgAll = 0;
        errCntAccAvgAll = 0;
        errCoverageAll = 0;
        errCenterAll = 0;
        
        lenALL = 0;
        anno=subAnno{1};
        
        successNumOverlap = zeros(1,length(thresholdSetOverlap));
        successNumErr = zeros(1,length(thresholdSetError));
            
        len = size(anno,1);
        [aveCoverage, aveErrCenter, errCoverage, errCenter] = calcSeqErrOurs(rectMat, anno);

        for tIdx=1:length(thresholdSetOverlap)
            successNumOverlap(1,tIdx) = sum(errCoverage >thresholdSetOverlap(tIdx));
        end

        for tIdx=1:length(thresholdSetError)
            successNumErr(1,tIdx) = sum(errCenter <= thresholdSetError(tIdx));
        end

        lenALL = lenALL + len;
        
        aveSuccessRatePlot(idxTrk, idxSeq,:) = successNumOverlap/(lenALL+eps);
        aveSuccessRatePlotErr(idxTrk, idxSeq,:) = successNumErr/(lenALL+eps);
        
    end
end
%
% dataName1=[perfMatPath 'aveSuccessRatePlot_' num2str(numTrk) 'alg_overlap_' evalType '.mat'];
% save(dataName1,'aveSuccessRatePlot','nameTrkAll');
% 
% dataName2=[perfMatPath 'aveSuccessRatePlot_' num2str(numTrk) 'alg_error_' evalType '.mat'];
% aveSuccessRatePlot = aveSuccessRatePlotErr;
% save(dataName2,'aveSuccessRatePlot','nameTrkAll');
