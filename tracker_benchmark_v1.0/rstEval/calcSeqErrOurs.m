function [aveErrCoverage, aveErrCenter,errCoverage, errCenter] = calcSeqErrOurs(rectMat, rect_anno)

% rectMat: n x 4 (x_lt, y_lt, w, h)
%
% LineWidth = 2;
% LineStyle = '-';%':';%':' '.-'
%
% lostCount = zeros(length(seqs), length(trks));
% thred = 0.33;
% 
% errCenterAll=[];
% errCoverageAll=[];

%% Preprocessing

seq_length = min( size(rectMat,1), size(rect_anno,1) );

for i = 2:seq_length
    r = rectMat(i,:);
    r_anno = rect_anno(i,:);
    if (isnan(r) | r(3)<=0 | r(4)<=0) & (~isnan(r_anno))
        rectMat(i,:) = rectMat(i-1,:);
    end
end

%% Computing

centerGT = [rect_anno(:,1)+(rect_anno(:,3)-1)/2 rect_anno(:,2)+(rect_anno(:,4)-1)/2];
rectMat(1,:) = rect_anno(1,:);

center = [rectMat(:,1)+(rectMat(:,3)-1)/2 rectMat(:,2)+(rectMat(:,4)-1)/2];

errCenter = sqrt(sum(((center(1:seq_length,:) - centerGT(1:seq_length,:)).^2),2));

index = rect_anno>0;
idx=(sum(index,2)==4);
% errCoverage = calcRectInt(rectMat(1:seq_length,:),rect_anno(1:seq_length,:));
tmp = calcRectInt(rectMat(idx,:),rect_anno(idx,:));

errCoverage=-ones(length(idx),1);
errCoverage(idx) = tmp;
errCenter(~idx)=-1;

aveErrCoverage = sum(errCoverage(idx))/length(idx);
aveErrCenter = sum(errCenter(idx))/length(idx);


