function genPerfMat(seqs, trackers, trackers_ours, evalType, nameTrkAll, perfMatPath)

[suc_o, sucerr_o] = genPerfMatOurs(seqs, trackers_ours);
[suc, sucerr] = genPerfMatBen(seqs, trackers, evalType);

nameTrkAll = [nameTrkAll; trackers_ours];
numTrk = length(nameTrkAll);
aveSuccessRatePlot = cat(1, suc, suc_o);
aveSuccessRatePlotErr = cat(1, sucerr, sucerr_o);

dataName1=[perfMatPath 'aveSuccessRatePlot_' num2str(numTrk) 'alg_overlap_' evalType '.mat'];
save(dataName1,'aveSuccessRatePlot','nameTrkAll');

dataName2=[perfMatPath 'aveSuccessRatePlot_' num2str(numTrk) 'alg_error_' evalType '.mat'];
aveSuccessRatePlot = aveSuccessRatePlotErr;
save(dataName2,'aveSuccessRatePlot','nameTrkAll');

end