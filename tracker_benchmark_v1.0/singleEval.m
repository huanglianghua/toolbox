function scores = singleEval(pathTracker, pathAnno)

if nargin < 2
    pathAnno = './anno/';
end

k = 1;
seqs = dir([pathAnno '*.txt']);

for i = 1:length(seqs)
    seq = seqs(i).name;
    title = seq(1:end-4);
    
    if ~exist([pathTracker seq],'file')
        continue;
    end
    
    rectAnno = dlmread([pathAnno seq]);
    rectTracker = dlmread([pathTracker seq]);
    
    seq_length = min(size(rectAnno,1), size(rectTracker,1));
    disp([title ' ' num2str(seq_length)]);
    
    centerAnno = [rectAnno(:,1)+(rectAnno(:,3)-1)/2 rectAnno(:,2)+(rectAnno(:,4)-1)/2];
    centerTracker = [rectTracker(:,1)+(rectTracker(:,3)-1)/2 rectTracker(:,2)+(rectTracker(:,4)-1)/2];
    errCenter = sqrt(sum(((centerAnno(1:seq_length,:) - centerTracker(1:seq_length,:)).^2),2));
    
    precision_scores(k,1) = sum(errCenter < 20) / seq_length;
    
    overlap = calcRectInt(rectAnno(1:seq_length,:), rectTracker(1:seq_length,:));
    overlap_scores(k,1) = mean(overlap);
    
    seq_names{k,1} = title;
    
    k = k + 1;
end

scores = seq_names;
for i = 1:k-1
    scores{i,2} = precision_scores(i,1);
    scores{i,3} = overlap_scores(i,1);
end

[~,idx] = sort(precision_scores, 'ascend');
scores = scores(idx,:);

end